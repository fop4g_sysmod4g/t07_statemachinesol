#include <assert.h>
#include <string>

#include "SFML/Graphics.hpp"


using namespace sf;
using namespace std;

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};


/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width,
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 800,600 };
}

bool LoadTexture(const string& file, Texture& tex)
{
	if (tex.loadFromFile(file))
	{
		tex.setSmooth(true);
		return true;
	}
	assert(false);
	return false;
}

void DrawCentred(Texture& bgnd, Sprite& spr, RenderWindow& window, const Vector2f& scale)
{
	spr.setTexture(bgnd);
	spr.setTextureRect(IntRect(0, 0, bgnd.getSize().x, bgnd.getSize().y));
	spr.setOrigin(bgnd.getSize().x / 2.f, bgnd.getSize().y / 2.f);
	spr.setPosition(window.getSize().x / 2.f, window.getSize().y / 2.f);
	spr.setScale(scale);
	window.draw(spr);
}


void DrawFullScreen(Texture& bgnd, Sprite& spr, RenderWindow& window)
{
	float xs = window.getSize().x / (float)bgnd.getSize().x;
	float ys = window.getSize().y / (float)bgnd.getSize().y;
	DrawCentred(bgnd, spr, window, Vector2f(xs, ys));
}

/*
Just a demo, a real game would need putting in its own module and
commenting appropraitely, etc.
*/
struct MyGame
{
	const int NUM_MEN = 5;
	const float MEN_SCALE = 0.25f;
	const float MEN_SPACING = 50;
	const Dim2Df MEN_OFF{ 30,30 };
	enum class State { START, GAME, OVER };
	State state = State::START;
	Texture ramTex;
	Texture startTex;
	Texture gameTex;
	Texture overTex;
	Sprite spr;
	Keyboard::Key keyRelease = Keyboard::Tilde;

	void KeyRelease(Keyboard::Key key)
	{
		keyRelease = key;
	}

	void Start(RenderWindow& window)
	{
		DrawFullScreen(startTex, spr, window);
		if (keyRelease == Keyboard::Space)
			state = State::GAME;
	}

	void Init()
	{
		LoadTexture("data/rampage.png", ramTex);
		LoadTexture("data/start1.png", startTex);
		LoadTexture("data/in_game.jpg", gameTex);
		LoadTexture("data/game_over.png", overTex);
	}

	void Game(RenderWindow& window)
	{
		DrawFullScreen(gameTex, spr, window);

		Sprite spr2(ramTex);
		spr2.setScale(MEN_SCALE, MEN_SCALE);
		const float space = MEN_SPACING, xOff = MEN_OFF.x, yOff = MEN_OFF.y;
		for (int y = 0; y < NUM_MEN; ++y)
			for (int x = 0; x < NUM_MEN; ++x)
			{
				spr2.setPosition(x*space + xOff, y*space + yOff);
				window.draw(spr2);
			}

		if (keyRelease == Keyboard::Space)
			state = State::OVER;
	}

	void Over(RenderWindow& window)
	{
		DrawCentred(overTex, spr, window, Vector2f(1,1));

		if (keyRelease == Keyboard::Space)
			state = State::START;
	}

	void Update(RenderWindow& window)
	{
		switch (state)
		{
		case State::START:
			Start(window);
			break;
		case State::GAME:
			Game(window);
			break;
		case State::OVER:
			Over(window);
			break;
		}
		keyRelease = Keyboard::Key::Tilde;
	}
};



int main()
{
	// Create the main window
	RenderWindow window(VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "My first code");


	MyGame myGame;
	myGame.Init();

	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		Event event;
		while (window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == Event::Closed) 
				window.close();
			if (event.type == Event::KeyReleased)
			{
				if (event.key.code == Keyboard::Escape)
					window.close();
				else
					myGame.KeyRelease(event.key.code);
			}
		} 

		// Clear screen
		window.clear();

				
		myGame.Update(window);

		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
